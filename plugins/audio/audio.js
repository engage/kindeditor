/*******************************************************************************
 * KindEditor - WYSIWYG HTML Editor for Internet
 * Copyright (C) 2006-2011 kindsoft.net
 *
 * @author Roddy <luolonghao@gmail.com>
 * @site http://www.kindsoft.net/
 * @licence http://www.kindsoft.net/license.php
 *******************************************************************************/

KindEditor.plugin('audio', function (K) {
    var self = this, name = 'audio', lang = self.lang(name + '.'),
        allowAudioUpload = K.undef(self.allowAudioUpload, true),
        allowFileManager = K.undef(self.allowFileManager, false),
        formatUploadUrl = K.undef(self.formatUploadUrl, true),
        extraParams = K.undef(self.extraFileUploadParams, {}),
        filePostName = K.undef(self.filePostName, 'imgFile'),
        uploadJson = K.undef(self.uploadJson, self.basePath + 'php/upload_json.php');
    self.plugin.audio = {
        edit: function () {

            var html = '<div class="ke-form">' +
                '<div class="ke-form-help">' + lang.tip + '</div>' +
                '<div class="ke-form-group">' +
                '<label class="ke-form-label">' + lang.url + '</label>' +
                '<div class="ke-form-controls ke-flex">' +
                '<input class="ke-input ke-flex-auto" type="text" name="url"><button name="upload" type="button" class="ke-button ke-upload-button">' + lang.upload + '</button><button name="viewServer" type="button" class="ke-button">' + lang.viewServer + '</button>' +
                '</div>' +
                '</div>' +
                '<div class="ke-form-group">' +
                '<label class="ke-form-label">' + lang.width + '</label>' +
                '<div class="ke-form-controls">' +
                '<div class="ke-flex-auto"><input type="text" class="ke-input" name="width" value="300"  />' +
                '</div>' +
                '</div>' +
                '<div class="ke-form-group">' +
                '<label class="ke-form-label" for="keAutostart">' + lang.autostart + '</label>' +
                '<div class="ke-form-controls ke-flex">' +
                '<input type="checkbox" class="ke-checkbox" id="keAutostart" name="autostart" value="" />' +
                '</div>' +
                '</div>' +
                '<div class="ke-form-group">' +
                '<label class="ke-form-label" for="keLoop">' + lang.loop + '</label>' +
                '<div class="ke-form-controls ke-flex">' +
                '<input type="checkbox" class="ke-checkbox" id="keLoop" name="loop" value="" />' +
                '</div>' +
                '</div>' +
                '</div>';

            var dialog = self.createDialog({
                    name: name,
                    width: 450,
                    height: 310,
                    title: self.lang(name),
                    body: html,
                    yesBtn: {
                        name: self.lang('yes'),
                        click: function (e) {
                            var url = K.trim(urlBox.val()),
                                width = widthBox.val(),
                                height = heightBox.val();
                            if (url == 'http://' || K.invalidUrl(url)) {
                                alert(self.lang('invalidUrl'));
                                urlBox[0].focus();
                                return;
                            }
                            if (!/^\d*$/.test(width)) {
                                alert(self.lang('invalidWidth'));
                                widthBox[0].focus();
                                return;
                            }
                            if (!/^\d*$/.test(height)) {
                                alert(self.lang('invalidHeight'));
                                heightBox[0].focus();
                                return;
                            }
                            if (!/(mp3|wav|ogg)(\?|$)/i.test(url)) {
                                urlBox[0].focus();
                                alert(lang.tip);
                                return;
                            }

                            var html = K.mediaImg(self.themesPath + 'common/blank.gif', {
                                src: url,
                                type: K.mediaType(url),
                                width: width,
                                height: height,
                                autostart: loopBox[0].checked ? 'true' : 'false',
                                loop: autostartBox[0].checked ? 'true' : 'false'
                            });
                            self.insertHtml(html).hideDialog().focus();
                        }
                    }
                }),
                div = dialog.div,
                urlBox = K('[name="url"]', div),
                viewServerBtn = K('[name="viewServer"]', div),
                widthBox = K('[name="width"]', div),
                heightBox = K('[name="height"]', div),
                autostartBox = K('[name="autostart"]', div);
                loopBox = K('[name="loop"]', div);
            urlBox.val('http://');

            if (allowAudioUpload) {
                var uploadbutton = K.uploadbutton({
                    button: K('.ke-upload-button', div)[0],
                    fieldName: filePostName,
                    extraParams: extraParams,
                    url: K.addParam(uploadJson, 'dir=audio'),
                    afterUpload: function (data) {
                        dialog.hideLoading();
                        if (data.error === 0) {
                            var url = data.url;
                            if (formatUploadUrl) {
                                url = K.formatUrl(url, 'absolute');
                            }
                            urlBox.val(url);
                            if (self.afterUpload) {
                                self.afterUpload.call(self, url, data, name);
                            }
                            alert(self.lang('uploadSuccess'));
                        } else {
                            alert(data.message);
                        }
                    },
                    afterError: function (html) {
                        dialog.hideLoading();
                        self.errorDialog(html);
                    }
                });
                uploadbutton.fileBox.change(function (e) {
                    dialog.showLoading(self.lang('uploadLoading'));
                    uploadbutton.submit();
                });
            } else {
                K('.ke-upload-button', div).hide();
            }

            if (allowFileManager) {
                viewServerBtn.click(function (e) {
                    self.loadPlugin('filemanager', function () {
                        self.plugin.filemanagerDialog({
                            viewType: 'LIST',
                            dirName: 'audio',
                            clickFn: function (url, title) {
                                if (self.dialogs.length > 1) {
                                    K('[name="url"]', div).val(url);
                                    if (self.afterSelectFile) {
                                        self.afterSelectFile.call(self, url);
                                    }
                                    self.hideDialog();
                                }
                            }
                        });
                    });
                });
            } else {
                viewServerBtn.hide();
            }

            var img = self.plugin.getSelectedAudio();
            if (img) {
                var attrs = K.mediaAttrs(img.attr('data-ke-tag'));
                urlBox.val(attrs.src);
                widthBox.val(K.removeUnit(img.css('width')) || attrs.width || 0);
                heightBox.val(K.removeUnit(img.css('height')) || attrs.height || 0);
                autostartBox[0].checked = (attrs.autostart === 'true');
                loopBox[0].checked = (attrs.loop === 'true');
            }
            urlBox[0].focus();
            urlBox[0].select();
        },
        'delete': function () {
            self.plugin.getSelectedAudio().remove();
            // [IE] 删除图片后立即点击图片按钮出错
            self.addBookmark();
        }
    };
    self.clickToolbar(name, self.plugin.audio.edit);
});
