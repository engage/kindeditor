/*******************************************************************************
* KindEditor - WYSIWYG HTML Editor for Internet
* Copyright (C) 2006-2011 kindsoft.net
*
* @author Roddy <luolonghao@gmail.com>
* @site http://www.kindsoft.net/
* @licence http://www.kindsoft.net/license.php
*******************************************************************************/

KindEditor.plugin('media', function(K) {
	var self = this, name = 'media', lang = self.lang(name + '.');
	self.plugin.media = {
		edit : function() {

            var html = '<div class="ke-form">' +
				'<div class="ke-form-help">' + lang.tip + '</div>' +
                '<div class="ke-form-group">' +
                '<label class="ke-form-label">' + lang.url + '</label>' +
                '<div class="ke-form-controls">' +
                '<input class="ke-input ke-flex-auto" type="text" name="url">' +
                '</div>' +
                '</div>' +
                '<div class="ke-form-group">' +
                '<label class="ke-form-label">' + lang.size + '</label>' +
                '<div class="ke-form-controls ke-flex">' +
                '<div>'+lang.width+'</div><div class="ke-flex-auto"><input type="text" class="ke-input" name="width" value="550"  /></div>' +
                '<div>'+lang.height+'</div><div class="ke-flex-auto"><input type="text" class="ke-input" name="height" value="400"  /></div>' +
                '</div>' +
                '</div>' +
                '<div class="ke-form-group">' +
                '<label class="ke-form-label" for="keAutostart">' + lang.autostart + '</label>' +
                '<div class="ke-form-controls ke-flex">' +
                '<input type="checkbox" class="ke-checkbox" id="keAutostart" name="autostart" value="" />' +
                '</div>' +
                '</div>' +
                '</div>';


			var convertUrl = function(url){
                if ( !url ) return '';
                url = K.trim(url)
                    .replace(/v\.youku\.com\/v_show\/id_([\w\-=]+)\.html.*$/i, 'player.youku.com/player.php/sid/$1/v.swf')
                    .replace(/(www\.)?youtube\.com\/watch\?v=([\w\-]+)/i, "www.youtube.com/v/$2")
                    .replace(/youtu.be\/(\w+)$/i, "www.youtube.com/v/$1")
                    .replace(/v\.ku6\.com\/.+\/([\w\.]+)\.html.*$/i, "player.ku6.com/refer/$1/v.swf")
                    .replace(/www\.56\.com\/u\d+\/v_([\w\-]+)\.html/i, "player.56.com/v_$1.swf")
                    .replace(/www.56.com\/w\d+\/play_album\-aid\-\d+_vid\-([^.]+)\.html/i, "player.56.com/v_$1.swf")
                    .replace(/v\.pps\.tv\/play_([\w]+)\.html.*$/i, "player.pps.tv/player/sid/$1/v.swf")
                    .replace(/www\.letv\.com\/ptv\/vplay\/([\d]+)\.html.*$/i, "i7.imgs.letv.com/player/swfPlayer.swf?id=$1&autoplay=0")
                    .replace(/www\.tudou\.com\/programs\/view\/([\w\-]+)\/?/i, "www.tudou.com/v/$1")
                    .replace(/v\.qq\.com\/x\/cover\/([\w]+)\.html.*$/i, "imgcache.qq.com/tencentvideo_v1/playerv3/TPout.swf?vid=$1")
                    .replace(/v\.qq\.com\/.+[\?\&]vid=([^&]+).*$/i, "imgcache.qq.com/tencentvideo_v1/playerv3/TPout.swf?vid=$1")
                    .replace(/v\.qq\.com\/x\/page\/([\w]+)\.html.*$/i, "imgcache.qq.com/tencentvideo_v1/playerv3/TPout.swf?vid=$1")
                    .replace(/my\.tv\.sohu\.com\/[\w]+\/[\d]+\/([\d]+)\.shtml.*$/i, "share.vrs.sohu.com/my/v.swf&id=$1");
                return url;
            };
			var dialog = self.createDialog({
				name : name,
				width : 450,
				height : 290,
				title : self.lang(name),
				body : html,
				yesBtn : {
					name : self.lang('yes'),
					click : function(e) {
						var source = K.trim(urlBox.val());
						var url = convertUrl(urlBox.val()),
							width = widthBox.val(),
							height = heightBox.val();

						if (url == 'http://' || K.invalidUrl(url)) {
							alert(self.lang('invalidUrl'));
							urlBox[0].focus();
							return;
						}
						if (!/^\d*$/.test(width)) {
							alert(self.lang('invalidWidth'));
							widthBox[0].focus();
							return;
						}
						if (!/^\d*$/.test(height)) {
							alert(self.lang('invalidHeight'));
							heightBox[0].focus();
							return;
						}

						var html = K.mediaImg(self.themesPath + 'common/blank.gif', {
								source : source,
								src : url,
								type : 'application/x-shockwave-flash',
								width : width,
								height : height,
								autostart : autostartBox[0].checked ? 'true' : 'false',
								loop : 'true'
							});
						self.insertHtml(html).hideDialog().focus();
					}
				}
			}),
			div = dialog.div,
			urlBox = K('[name="url"]', div),
			widthBox = K('[name="width"]', div),
			heightBox = K('[name="height"]', div),
			autostartBox = K('[name="autostart"]', div);
			urlBox.val('http://');


			var img = self.plugin.getSelectedMedia();
			if (img) {
				var attrs = K.mediaAttrs(img.attr('data-ke-tag'));
				urlBox.val(attrs.source);
				widthBox.val(K.removeUnit(img.css('width')) || attrs.width || 0);
				heightBox.val(K.removeUnit(img.css('height')) || attrs.height || 0);
				autostartBox[0].checked = (attrs.autostart === 'true');
			}
			urlBox[0].focus();
			urlBox[0].select();
		},
		'delete' : function() {
			self.plugin.getSelectedMedia().remove();
			// [IE] 删除图片后立即点击图片按钮出错
			self.addBookmark();
		}
	};
	self.clickToolbar(name, self.plugin.media.edit);
});
