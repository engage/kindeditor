/*******************************************************************************
* KindEditor - WYSIWYG HTML Editor for Internet
* Copyright (C) 2006-2011 kindsoft.net
*
* @author Roddy <luolonghao@gmail.com>
* @site http://www.kindsoft.net/
* @licence http://www.kindsoft.net/license.php
*******************************************************************************/

KindEditor.plugin('image', function(K) {
	var self = this, name = 'image',
		allowImageUpload = K.undef(self.allowImageUpload, true),
		allowImageRemote = K.undef(self.allowImageRemote, true),
		formatUploadUrl = K.undef(self.formatUploadUrl, true),
		allowFileManager = K.undef(self.allowFileManager, false),
		uploadJson = K.undef(self.uploadJson, self.basePath + 'php/upload_json.php'),
		imageTabIndex = K.undef(self.imageTabIndex, 0),
		imgPath = self.pluginsPath + 'image/images/',
		extraParams = K.undef(self.extraFileUploadParams, {}),
		filePostName = K.undef(self.filePostName, 'imgFile'),
		fillDescAfterUploadImage = K.undef(self.fillDescAfterUploadImage, false),
		lang = self.lang(name + '.');

	self.plugin.imageDialog = function(options) {
		var imageUrl = options.imageUrl,
			imageWidth = K.undef(options.imageWidth, ''),
			imageHeight = K.undef(options.imageHeight, ''),
			imageTitle = K.undef(options.imageTitle, ''),
			imageAlign = K.undef(options.imageAlign, ''),
			showRemote = K.undef(options.showRemote, true),
			clickFn = options.clickFn;
		var target = 'kindeditor_upload_iframe_' + new Date().getTime();
		var hiddenElements = [];
		for(var k in extraParams){
			hiddenElements.push('<input type="hidden" name="' + k + '" value="' + extraParams[k] + '" />');
		}

		var html = '<div class="ke-form">' +
				'<div class="ke-form-group">' +
					'<label class="ke-form-label">' + lang.remoteUrl + '</label>' +
					'<div class="ke-form-controls ke-flex">' +
						'<input class="ke-input ke-flex-auto" type="text" name="url"><button name="upload" type="button" class="ke-button ke-upload-button">' + lang.upload + '</button><button name="viewServer" type="button" class="ke-button">' + lang.viewServer + '</button>' +
					'</div>' +
				'</div>' +
				'<div class="ke-form-group">' +
					'<label class="ke-form-label">' + lang.size + '</label>' +
					'<div class="ke-form-controls ke-flex">' +
						'<div>'+lang.width+'</div><div class="ke-flex-auto"><input type="text" class="ke-input" name="width" value=""  /></div>' +
            			'<div>'+lang.height+'</div><div class="ke-flex-auto"><input type="text" class="ke-input" name="height" value=""  /></div>' +
            			'<div><img class="ke-refresh-btn" src="' + imgPath + 'refresh.png" width="16" height="16" alt="" style="cursor:pointer;" title="' + lang.resetSize + '" /></div>' +
					'</div>' +
				'</div>' +
				'<div class="ke-form-group">' +
					'<label class="ke-form-label">' + lang.align + '</label>' +
					'<div class="ke-form-controls ke-flex">' +
						'<label class="ke-flex-auto"><input type="radio" name="align" value="" checked="checked" /> 居中对齐</label>' +
            			'<label class="ke-flex-auto"><input type="radio" name="align" value="left" /> 左对齐</label>' +
            			'<label class="ke-flex-auto"><input type="radio" name="align" value="right"  /> 右对齐</label>' +
					'</div>' +
				'</div>' +
				'<div class="ke-form-group">' +
					'<label class="ke-form-label">' + lang.imgTitle + '</label>' +
					'<div class="ke-form-controls">' +
						'<input class="ke-input ke-flex-auto" type="text" name="title">' +
					'</div>' +
				'</div>' +
			'</div>';

		var dialogWidth = 450,
			dialogHeight = 280;
		var dialog = self.createDialog({
			name : name,
			width : dialogWidth,
			height : dialogHeight,
			title : self.lang(name),
			body : html,
			yesBtn : {
				name : self.lang('yes'),
				click : function(e) {
					// Bugfix: http://code.google.com/p/kindeditor/issues/detail?id=319
					if (dialog.isLoading) {
						return;
					}
					// insert remote image
					var url = K.trim(urlBox.val()),
						width = widthBox.val(),
						height = heightBox.val(),
						title = titleBox.val(),
						align = '';
					alignBox.each(function() {
						if (this.checked) {
							align = this.value;
							return false;
						}
					});
					if (url == 'http://' || K.invalidUrl(url)) {
						alert(self.lang('invalidUrl'));
						urlBox[0].focus();
						return;
					}
					if (!/^\d*$/.test(width)) {
						alert(self.lang('invalidWidth'));
						widthBox[0].focus();
						return;
					}
					if (!/^\d*$/.test(height)) {
						alert(self.lang('invalidHeight'));
						heightBox[0].focus();
						return;
					}
					clickFn.call(self, url, title, width, height, 0, align);
				}
			},
			beforeRemove : function() {
				viewServerBtn.unbind();
				widthBox.unbind();
				heightBox.unbind();
				refreshBtn.unbind();
			}
		}),
		div = dialog.div;

		var urlBox = K('[name="url"]', div),
			localUrlBox = K('[name="localUrl"]', div),
			uploadBtn = K('[name="upload"]', div),
			viewServerBtn = K('[name="viewServer"]', div),
			widthBox = K('.[name="width"]', div),
			heightBox = K('[name="height"]', div),
			refreshBtn = K('.ke-refresh-btn', div),
			titleBox = K('.[name="title"]', div),
			alignBox = K('[name="align"]', div);

        if (allowImageUpload) {
            var uploadbutton = K.uploadbutton({
                button : uploadBtn[0],
                fieldName : filePostName,
                url : K.addParam(uploadJson, 'dir=image'),
                extraParams : extraParams,
                afterUpload : function(data) {
                    dialog.hideLoading();
                    if (data.error === 0) {
                        var url = data.url;
                        if (formatUploadUrl) {
                            url = K.formatUrl(url, 'absolute');
                        }
                        urlBox.val(url);
                        if (self.afterUpload) {
                            self.afterUpload.call(self, url, data, name);
                        }
                        alert(self.lang('uploadSuccess'));
                    } else {
                        alert(data.message);
                    }
                },
                afterError : function(html) {
                    dialog.hideLoading();
                    self.errorDialog(html);
                }
            });
            uploadbutton.fileBox.change(function(e) {
                dialog.showLoading(self.lang('uploadLoading'));
                uploadbutton.submit();
            });
        } else {
            K('.ke-upload-button', div).hide();
        }

		if (allowFileManager) {
			viewServerBtn.click(function(e) {
				self.loadPlugin('filemanager', function() {
					self.plugin.filemanagerDialog({
						viewType : 'VIEW',
						dirName : 'image',
						clickFn : function(url, title) {
							if (self.dialogs.length > 1) {
								K('[name="url"]', div).val(url);
								if (self.afterSelectFile) {
									self.afterSelectFile.call(self, url);
								}
								self.hideDialog();
							}
						}
					});
				});
			});
		} else {
			viewServerBtn.hide();
		}
		var originalWidth = 0, originalHeight = 0;
		function setSize(width, height) {
			widthBox.val(width);
			heightBox.val(height);
			originalWidth = width;
			originalHeight = height;
		}
		refreshBtn.click(function(e) {
			var tempImg = K('<img src="' + urlBox.val() + '" />', document).css({
				position : 'absolute',
				visibility : 'hidden',
				top : 0,
				left : '-1000px'
			});
			tempImg.bind('load', function() {
				setSize(tempImg.width(), tempImg.height());
				tempImg.remove();
			});
			K(document.body).append(tempImg);
		});
		widthBox.change(function(e) {
			if (originalWidth > 0) {
				heightBox.val(Math.round(originalHeight / originalWidth * parseInt(this.value, 10)));
			}
		});
		heightBox.change(function(e) {
			if (originalHeight > 0) {
				widthBox.val(Math.round(originalWidth / originalHeight * parseInt(this.value, 10)));
			}
		});
		urlBox.val(options.imageUrl);
		setSize(options.imageWidth, options.imageHeight);
		titleBox.val(options.imageTitle);
		alignBox.each(function() {
			if (this.value === options.imageAlign) {
				this.checked = true;
				return false;
			}
		});
		if (showRemote) {
			urlBox[0].focus();
			urlBox[0].select();
		}
		return dialog;
	};
	self.plugin.image = {
		edit : function() {
			var img = self.plugin.getSelectedImage();
			self.plugin.imageDialog({
				imageUrl : img ? img.attr('data-ke-src') : 'http://',
				imageWidth : img ? img.width() : '',
				imageHeight : img ? img.height() : '',
				imageTitle : img ? img.attr('title') : '',
				imageAlign : img ? img.attr('align') : '',
				showRemote : allowImageRemote,
				//showLocal : allowImageUpload,
				clickFn : function(url, title, width, height, border, align) {
					if (img) {
						img.attr('src', url);
						img.attr('data-ke-src', url);
						img.attr('width', width);
						img.attr('height', height);
						img.attr('title', title);
						img.attr('align', align);
						img.attr('alt', title);
					} else {
						self.exec('insertimage', url, title, width, height, border, align);
					}
					// Bugfix: [Firefox] 上传图片后，总是出现正在加载的样式，需要延迟执行hideDialog
					setTimeout(function() {
						self.hideDialog().focus();
					}, 0);
				}
			});
		},
		'delete' : function() {
			var target = self.plugin.getSelectedImage();
			if (target.parent().name == 'a') {
				target = target.parent();
			}
			target.remove();
			// [IE] 删除图片后立即点击图片按钮出错
			self.addBookmark();
		}
	};
	self.clickToolbar(name, self.plugin.image.edit);
});
